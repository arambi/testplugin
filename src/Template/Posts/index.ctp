<?php foreach( $posts as $post): ?>
    <div>
      <?= $this->Html->link( $post->title, [
        'plugin' => 'Blog',
        'controller' => 'Posts',
        'action' => 'view',
        'slug' => $post->slug,
        'section_id' => $this->request->params ['section']->id,
        'content_id' => $this->request->params ['section']->content_id
      ]) ?>
    </div>
<?php endforeach ?>