<?php foreach( $rows as $row): ?>
  <div class="row">
    <?php foreach( $row->blocks as $block): ?>
      <div class="col-md-<?= $block->cols ?>">
        <?= $this->cell( $block->block_type ['cell'], ['block' => $block, 'content' => $content]) ?>
      </div>
    <?php endforeach ?>
  </div>
<?php endforeach ?>